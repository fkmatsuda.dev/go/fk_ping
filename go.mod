module fk_ping

go 1.16

require (
	github.com/mitchellh/go-homedir v1.1.0 // indirect
	github.com/spf13/cobra v1.2.1 // indirect
	github.com/spf13/viper v1.9.0 // indirect
	golang.org/x/sys v0.0.0-20211007075335-d3039528d8ac // indirect
	golang.org/x/text v0.3.7 // indirect
)
