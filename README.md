# fk_ping

A simple HTTP service that responds with a status of 200 and a text "ok", this can be used to verify that nodes in a cluster are up.