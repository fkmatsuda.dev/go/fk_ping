package ping

import (
	"fk_ping/src/config"
	"fmt"
	"net/http"
)

/*
	Listening inits listening mode.
*/
func Listening() {
	http.HandleFunc(config.Path, pingServer)
	http.ListenAndServe(fmt.Sprintf(":%d", config.Port), nil)
}

func pingServer(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "ok")
}
