package ping

import (
	"errors"
	"fk_ping/src/config"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
)

func Ping(host string, ssl, verbose bool) {
	if verbose {
		useSSL := "no"
		if ssl {
			useSSL = "yes"
		}
		fmt.Printf("ping to %s\nssl: %s\n", host, useSSL)

	}

	proto := "http"
	if ssl {
		proto = "https"
	}

	resp, err := http.Get(fmt.Sprintf("%s://%s:%d%s", proto, host, config.Port, config.Path))

	if err != nil {
		if !verbose {
			os.Exit(1)
		}
		log.Fatal(err)
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)

	if err != nil {
		if !verbose {
			os.Exit(1)
		}
		log.Fatal(err)
	}
	strBody := string(body)

	if verbose {
		log.Println(strBody)
	}

	if strBody != "ok" {
		if verbose {
			log.Fatal(errors.New("not ok"))
		}
		os.Exit(1)
	}

}
