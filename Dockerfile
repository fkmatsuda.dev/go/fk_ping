#build stage
FROM golang:1.17.2-alpine3.14 AS builder
RUN apk add --no-cache git
WORKDIR /go/src/app
COPY . .
RUN go get -d -v ./...
RUN go build -o /go/bin/app -v ./...

#final stage
FROM alpine:3.14
ENV PING_PORT=64666
ENV PING_PATH=/ping
RUN apk --no-cache add ca-certificates
COPY --from=builder /go/bin/app /app
ENTRYPOINT [ "/app", "l", "-p", "${PING_PORT}", "-d", "${PING_PATH}" ]
LABEL Name=fkping
EXPOSE ${PING_PORT}
